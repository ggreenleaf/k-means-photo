from PIL import Image
from inputs import InputType



def get_image_pixels(image):
    width, height = image.size
    pixels = []
    for i in range(width):
        for j in range(height):
             pixel = image.getpixel((i,j))
             pixels.append(InputType((i,j),pixel))
    return pixels

def get_image(filepath):
    image = Image.open(filepath)
    return image


def create_new_picture(filepath, width, height, pixels, original_means):
    sorted_pixels = list(sorted(pixels, key=lambda k: [k.position[0],k.position[1]]))
    image = Image.new("RGB", (width,height))
    for pixel in sorted_pixels:
        values = tuple(map(int, list(original_means[pixel.cluster_assignment])))
        image.putpixel(pixel.position, values)

    image.save(filepath)
