# from PIL import Image
#
# im = Image.open('pictures/original\monobean.jpg')
# im.show()
from random import randint
import image_parser as ip
from kmeans import Kmeans
from os import path
from sys import argv

def get_random_means(n):
    return [(randint(0,255),randint(0,255), randint(0,255)) for i in range(n)]

if __name__ == "__main__":
    image_path = argv[1]
    num_means = int(argv[2])
    image_name = path.split(image_path)[1]
    image = ip.get_image(image_path)
    pixels = ip.get_image_pixels(image)
    init_means = get_random_means(num_means)
    kmeans = Kmeans(pixels, init_means)
    kmeans.start()
    clusted_pixels = kmeans.inputs
    new_picture_path = path.join("pictures","clustered", image_name)
    width, height = image.size
    ip.create_new_picture(new_picture_path,width, height, clusted_pixels, init_means)
